﻿/**
 * Copyright (c) 2009 Matthew Douglas <douglasmak@ccsu.edu>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

using System;
using System.Collections.Generic;
using System.Text;

namespace Tiger
{
    class Program
    {
        static TigerHash tiger = new TigerHash();


        static void Main(string[] args)
        {
            Console.WriteLine("============ Tiger Hash v1.0 =============");
            Console.WriteLine("by Matthew Douglas <douglasmak@ccsu.edu>");
            Console.WriteLine("Central Connecticut State University");
            Console.WriteLine();

            Console.WriteLine("Initializing test vectors...");
            InitializeTests();

            Console.WriteLine("Beginning tests on Tiger/192");

            int count = 0;
            foreach (TestVector v in tests)
            {
                count++;

                Console.CursorLeft = 0;
                Console.Write("Testing Vector {0:d} of {1:d}...", count, tests.Count);

                string hash = tiger.CalculateString(v.Message);

                if (!hash.Equals(v.ExpectedHash))
                {
                    Console.WriteLine("Failed");
                    Console.WriteLine("Test={0}", v.Message);
                    Console.WriteLine("Expected={0}", v.ExpectedHash);
                    Console.WriteLine("Actual=  {0}", hash);
                    Console.WriteLine("Aborting...");
                    break;
                }
            }

            Console.WriteLine();
            Console.WriteLine("Tiger/192 Testing complete!");
            Console.WriteLine();

            Console.WriteLine("Beginning tests on Tiger2/192");

            count = 0;
            tiger.Mode = TigerHashMode.Tiger2_192;
            foreach (TestVector v in tests2)
            {
                count++;

                Console.CursorLeft = 0;
                Console.Write("Testing Vector {0:d} of {1:d}...", count, tests.Count);

                string hash = tiger.CalculateString(v.Message);

                if (!hash.Equals(v.ExpectedHash))
                {
                    Console.WriteLine("Failed");
                    Console.WriteLine("Test={0}", v.Message);
                    Console.WriteLine("Expected={0}", v.ExpectedHash);
                    Console.WriteLine("Actual=  {0}", hash);
                    Console.WriteLine("Aborting...");
                    break;
                }
            }

            Console.WriteLine();
            Console.WriteLine("Tiger2/192 Testing complete!");
            Console.WriteLine();

            bool alive = true;
            while (alive)
            {
                Console.Write("Enter a string to hash (!q to quit): ");
                string line = Console.ReadLine();

                if (line.Equals("!q"))
                {
                    alive = false;
                }
                else
                {
                    tiger.Mode = TigerHashMode.Tiger192;
                    string hash = tiger.CalculateString(line);
                    tiger.Mode = TigerHashMode.Tiger2_192;
                    string hash2 = tiger.CalculateString(line);

                    Console.WriteLine("Tiger/192= {0}", hash);
                    Console.WriteLine("Tiger2/192={0}", hash2);
                    Console.WriteLine();
                }
            }
        }

        #region Test Vectors
        class TestVector
        {
            private string message, expectedHash;
            public string Message { get { return this.message; } set { this.message = value; } }
            public string ExpectedHash { get { return this.expectedHash; } set { this.expectedHash = value; } }
            public TestVector(string message, string expectedHash)
            {
                this.message = message;
                this.expectedHash = expectedHash;
            }
        }

        static List<TestVector> tests = new List<TestVector>();
        static List<TestVector> tests2 = new List<TestVector>();

        static void InitializeTests()
        {
            // All vectors obtained from http://www.cs.technion.ac.il/~biham/Reports/Tiger/

            string zerobits = "\0";

            // Tiger/192, Set 1
            tests.Add(new TestVector("", "3293AC630C13F0245F92BBB1766E16167A4E58492DDE73F3"));
            tests.Add(new TestVector("a", "77BEFBEF2E7EF8AB2EC8F93BF587A7FC613E247F5F247809"));
            tests.Add(new TestVector("abc", "2AAB1484E8C158F2BFB8C5FF41B57A525129131C957B5F93"));
            tests.Add(new TestVector("message digest", "D981F8CB78201A950DCF3048751E441C517FCA1AA55A29F6"));
            tests.Add(new TestVector("abcdefghijklmnopqrstuvwxyz", "1714A472EEE57D30040412BFCC55032A0B11602FF37BEEE9"));
            tests.Add(new TestVector("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq", "0F7BF9A19B9C58F2B7610DF7E84F0AC3A71C631E7B53F78E"));
            tests.Add(new TestVector("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", "8DCEA680A17583EE502BA38A3C368651890FFBCCDC49A8CC"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, "1234567890", 8).ToString(), "1C14795529FD9F207A958F84C52F11E887FA0CABDFD91BFD"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, "a", 1000000).ToString(), "6DB0E2729CBEAD93D715C6A7D36302E9B3CEE0D2BC314B41"));

            // Tiger/192, selected from Set 2
            tests.Add(new TestVector(zerobits, "5D9ED00A030E638BDB753A6A24FB900E5A63B8E73E6C25B6"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 2).ToString(), "AABBCCA084ACECD0511D1F6232A17BFAEFA441B2982E5548"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 3).ToString(), "7F7A8CE9580077EB6FD53BE8BB3E70650E2FDD8DBB44F5C7"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 4).ToString(), "605D1B8C132BF5D16F1A8BC2451733F7F0FF57FD5F49E298"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 5).ToString(), "7F3A0954A5C374566C72370D1D97C2AC8FA9B1E3CB216E31"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 6).ToString(), "B333735AFDAB30B4B597CCD137AC1AD2B30D4A047BAD0127"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 7).ToString(), "1F27BB5926F35CF5D52A24817BF56CD17D79BB65ABE85785"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 8).ToString(), "5229DC51A494B913F8E04C4C729C93CB8B260CA4EE8EA9D7"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 16).ToString(), "464B87921CCDAEDBC0D6941610D1EB19E536036096403F32"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 32).ToString(), "739414BD4CD6AB967CD46A1D943412757D858B24D1C4ECF7"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 64).ToString(), "33FF9966DDD692427A9BC4D611F3C74CF629A0544A1A7ED7"));
            tests.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 96).ToString(), "05D2A28308C44536C591DF21B02AB542AF982B81A4C5E129"));
        
            // Tiger2/192, Set 1
            tests2.Add(new TestVector("", "4441BE75F6018773C206C22745374B924AA8313FEF919F41"));
            tests2.Add(new TestVector("a", "67E6AE8E9E968999F70A23E72AEAA9251CBC7C78A7916636"));
            tests2.Add(new TestVector("abc", "F68D7BC5AF4B43A06E048D7829560D4A9415658BB0B1F3BF"));
            tests2.Add(new TestVector("message digest", "E29419A1B5FA259DE8005E7DE75078EA81A542EF2552462D"));
            tests2.Add(new TestVector("abcdefghijklmnopqrstuvwxyz", "F5B6B6A78C405C8547E91CD8624CB8BE83FC804A474488FD"));
            tests2.Add(new TestVector("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq", "A6737F3997E8FBB63D20D2DF88F86376B5FE2D5CE36646A9"));
            tests2.Add(new TestVector("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", "EA9AB6228CEE7B51B77544FCA6066C8CBB5BBAE6319505CD"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, "1234567890", 8).ToString(), "D85278115329EBAA0EEC85ECDC5396FDA8AA3A5820942FFF"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, "a", 1000000).ToString(), "E068281F060F551628CC5715B9D0226796914D45F7717CF4"));

            // Tiger2/192, selected from Set 2
            tests2.Add(new TestVector(zerobits, "860F73A57072EFACBBEA35F7DB67D97AA829C53463805093"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 2).ToString(), "46DCB92C4217C36222915C85BCDE0EB0250A9B76C41CC9D2"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 3).ToString(), "834B5E6E248A6DF4F35B363E29CFBE1D8CDCE9AC55728649"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 4).ToString(), "79BC152719662CA815EE101856EAABED6BD2ECAFAEA2E21C"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 5).ToString(), "A8AF6FB4B6EBBDB0C498471189422F2E3E82B9893B4E5260"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 6).ToString(), "B721E30C243329CFBE0E88DAC6D0C1E75D1E21286086F667"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 7).ToString(), "085935327E118496D0E8EC8F1C1FF408A61358517862B603"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 8).ToString(), "F7EDA67C1820A912B17948A82D29A32A6EA1D2DDF9AD0B73"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 16).ToString(), "2134110B93B62F7507576D8677C46222BCA4445F9DFDC31C"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 32).ToString(), "74CEF5F3B0115CA02529B2CD1A36F857372268AE5056CD23"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 64).ToString(), "9AE268E25C6F98E0D8AA9859AA81681E2AA1442708670F73"));
            tests2.Add(new TestVector(new StringBuilder().Insert(0, zerobits, 96).ToString(), "8AFA59278B1C5E8DD4FB3E212FC5BBE5A2C2FA05A73316C2"));
        
        }
        #endregion
    }
}
